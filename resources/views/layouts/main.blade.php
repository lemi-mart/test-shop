<!DOCTYPE html>
<html>
	<head>
		<title>Главная страница</title>
		<meta charset="UTF-8">
	</head>
	<body style="width: 100%; height: 100%;">
        <div class="container">
			<!--{!!$view!!}-->
			@yield('content')
        </div>
		@include('layouts.scripts')		
    </body>
</html>