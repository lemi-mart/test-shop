@if(isset($orders_list) && !empty($orders_list))
<table class="orders">
	<tr>
		<th>Номер заказа</th>
		<th>Состав заказа</th>
		<th>Дата оформления</th>
	</tr >

	@foreach($orders_list as $key=>$orders)
	<tr>
				<td>Заказ № {{$key}}</td>
				<td style="padding: 0;border:none">
					
						@foreach($orders as $order)
						<ul class="order_items">
								@if($order->type_name)
									<li>{{$order->prod_name}}</li><br>
									<li>{{$order->type_name}}</li>
									<li style="float: right;font-weight: bold;display: block;margin-right: 20px;">{{$order->quantity}}  шт.</li><br><br>
								@else
									<li>{{$order->prod_name}}</li>
									<li style="float: right;font-weight: bold;display: block;margin-right: 20px;">{{$order->quantity}}  шт.</li><br><br>
								@endif
							</ul>
						@endforeach
					
				</td>
				<td>{{date('H:i d.m.Y ',strtotime($orders[0]->created_at))}}</td>				
			</tr>		
	@endforeach
</table>
@endif

