@extends('main')

@section('content')
<div class="basket">
	<input id="csrf_token" type="hidden" name="_token" value="{{ csrf_token() }}" />
	<h1>Корзина</h1>
	<div class="items_list">
		@if(!$basket_items)
			Корзина пуста
		@else
			@if(isset($items) && !empty($items))
			<ul class="head">
				<li>Изображение</li>
				<li>Наименование</li>
				<li>Количество</li>
				<li></li>
			</ul>
				@foreach($items as &$item)

				<ul id="item_{{$item->item_id}}" data-id='{{$item->item_id}}'>
					<li><a href="/product/{{$item->product_id}}"><img src="/picture.php?width=100&amp;height=100&amp;image=/uploads/{{$item->photo}}"></a></li>
					<li style="line-height: inherit;padding-top: 30px;box-sizing: border-box;"><a href="/product/{{$item->product_id}}">{{$item->product}}<br>
							{{$item->type_name}}</a></li>
					<li>
						<select class="item_quantity">
							<?php $item->quantity = $item->type_quantity?$item->type_quantity:$item->quantity ?>
							@for($i = 1;$i<=$item->quantity;$i++)
								<option <?php echo $i== $item->basket_quantity?'selected':''?> value="{{$i}}">{{$i}}</option>
							@endfor
						</select>
					</li>
					<li><span class="remove">Удалить</span></li>
				</ul>
				@endforeach			
			@endif
			<div id="go_order" class="btn">Офомрить заказ</div>
		@endif
	</div>
</div>
@endsection
