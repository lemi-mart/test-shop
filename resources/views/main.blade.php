<!DOCTYPE html>
<html lang="en">
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/css/style.css" rel="stylesheet">
		<link href="/css/jquery-ui.min.css" rel="stylesheet">
		<title>Главная страница</title>
		<meta charset="UTF-8">
	</head>
	<body>
		<nav class="main_menu">
			<div class="menu_wrap">
				<ul class="nav navbar-nav">
					<li style="background: #F54F29"><a href="/">главная</a></li>
					<li style="background: #98C654"><a href="/basket">корзина</a></li>
					<li style="background: #3598DB"><a href="/profile">заказы</a></li>
					<!--<li style="background: #6B4627"><a href="/profile">Профиль</a></li>-->
					<li style="background: #F4981F"><a href="">{{ Auth::user()->name }}</a></li>
					<li style="background: #392F42"><a href="/auth/logout">выйти</a></li>
					<div id="basket_info">Товаров в корзине: {!!$basket_items!!}</div>
				</ul>
			</div>
		</nav>
        <section class="content">
			@yield('content')
        </section>
			@include('scripts')
    </body>
</html>