@extends('main')

@section('content')
<div class="catalog">
	<h1>Каталог товаров</h1><br><br>
	@if ($products->count())
        @foreach($products as $prod)	
		<div class="item"><a href="/product/{{$prod->id}}">
			<img src="/picture.php?width=138&amp;height=138&amp;image=/uploads/{!!$prod->photo!!}"><br>
			<div style="margin-top: 10px;">{{ $prod->name }}</div><br>
			</a>
		</div>
        @endforeach
	@endif
</div>

@endsection
