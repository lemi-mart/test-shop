@extends('main')

@section('content')
<div class="basket">
	<input id="csrf_token" type="hidden" name="_token" value="{{ csrf_token() }}" />
	@if(isset($orders_list) && !empty($orders_list))
	<div class="orders_filter">
		Фильтр<br>
		
		с <input id="date_from" class="datepicker" type="text" name="date_from">
		по <input id="date_to" class="datepicker" type="text" name="date_to">
		&nbsp;&nbsp;Количество товаров в заказе: 
		<select id="count_items">
			<?php for($i=1;$i<20;$i++):?>
			<option value="{{$i}}">{{$i}}</option>
			<?php endfor;?>
		</select>
		<button id="go_filter">фильтровать</button>
	</div>
	<div id="orders_list">
	<table class="orders">
			<tr>
				<th>Номер заказа</th>
				<th>Состав заказа</th>
				<th>Дата оформления</th>
			</tr >
			
			@foreach($orders_list as $key=>$orders)
			<tr>
				<td>Заказ № {{$key}}</td>
				<td style="padding: 0;border:none">
					
						@foreach($orders as $order)
						<ul class="order_items">
								@if($order->type_name)
									<li>{{$order->prod_name}}</li><br>
									<li>{{$order->type_name}}</li>
									<li style="float: right;font-weight: bold;display: block;margin-right: 20px;">{{$order->quantity}}  шт.</li><br><br>
								@else
									<li>{{$order->prod_name}}</li>
									<li style="float: right;font-weight: bold;display: block;margin-right: 20px;">{{$order->quantity}}  шт.</li><br><br>
								@endif
							</ul>
						@endforeach
					
				</td>
				<td>{{date('H:i d.m.Y ',strtotime($orders[0]->created_at))}}</td>				
			</tr>		
			@endforeach
		</table>
	</div>
	@else
	<h2>Вы еще не сделали ни одного заказа</h2>
	@endif
</div>
@endsection
