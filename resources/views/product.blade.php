@extends('main')

@section('content')
<div class="product">
	<input id="csrf_token" type="hidden" name="_token" value="{{ csrf_token() }}" />
	<input id="product_id" type="hidden" name="product_id" value="{{ $product_id }}" />
	
	
	<h1>{{$products[0]->name}}</h1><br>
		<div class="photo_wrap">
			<img src="/picture.php?width=200&amp;height=200&amp;image=/uploads/{{$products[0]->photo}}">
		</div>		
		<div class="product_info">
			@if(!$is_available)
				<h3>Товара нет на складе</h3>
			@else
				@if (isset($products[0]->type_id) && $products[0]->type_id)
					<div style="display: inline-block">
						Тип товара:<br><br>
						<select id="type_id" name="type_id">
							@foreach($products as $prod)
								@if ($prod->type_quantity)
								<option value="{{$prod->type_id}}">{{$prod->type_name}}</option>
								@endif
						   @endforeach
					   </select>
						</div>
					<div style="display: inline-block;margin-left: 40px;">
					Количество:<br><br>
					<select id="quantity" name="quantity">
							@for($i = 1;$i<=$products[0]->type_quantity;$i++)
								<option value="{{$i}}">{{$i}}</option>
							@endfor
						</select>
					</div>
				@else
					Количество:<br>
					<select id="quantity" name="quantity">
					@for($i = 1;$i<=$products[0]->quantity;$i++)
						<option value="{{$i}}">{{$i}}</option>
					@endfor
					</select>					
				@endif
				<div id="add_to_basket" class="btn">в корзину</div>	
			@endif
	   </div>	
	<div class="clear"></div>
</div>
@endsection
