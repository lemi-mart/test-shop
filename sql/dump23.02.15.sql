--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.1
-- Dumped by pg_dump version 9.4.1
-- Started on 2015-02-23 21:29:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 184 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2061 (class 0 OID 0)
-- Dependencies: 184
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 179 (class 1259 OID 16443)
-- Name: basket; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE basket (
    id integer NOT NULL,
    user_id integer NOT NULL,
    prod_id integer NOT NULL,
    prod_type_id integer DEFAULT 0,
    quantity smallint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


--
-- TOC entry 178 (class 1259 OID 16441)
-- Name: basket_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE basket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2062 (class 0 OID 0)
-- Dependencies: 178
-- Name: basket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE basket_id_seq OWNED BY basket.id;


--
-- TOC entry 183 (class 1259 OID 16461)
-- Name: order_cart; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE order_cart (
    id integer NOT NULL,
    prod_name character varying(255) NOT NULL,
    type_name character varying(255),
    quantity smallint NOT NULL,
    order_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- TOC entry 182 (class 1259 OID 16459)
-- Name: order_cart_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_cart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2063 (class 0 OID 0)
-- Dependencies: 182
-- Name: order_cart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_cart_id_seq OWNED BY order_cart.id;


--
-- TOC entry 181 (class 1259 OID 16452)
-- Name: orders; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE orders (
    id integer NOT NULL,
    user_id integer NOT NULL,
    status smallint DEFAULT 0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- TOC entry 180 (class 1259 OID 16450)
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2064 (class 0 OID 0)
-- Dependencies: 180
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- TOC entry 177 (class 1259 OID 16432)
-- Name: product_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE product_types (
    id integer NOT NULL,
    name character varying NOT NULL,
    quantity smallint NOT NULL,
    prod_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- TOC entry 176 (class 1259 OID 16430)
-- Name: product_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE product_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2065 (class 0 OID 0)
-- Dependencies: 176
-- Name: product_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE product_types_id_seq OWNED BY product_types.id;


--
-- TOC entry 175 (class 1259 OID 16424)
-- Name: products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    quantity smallint,
    photo character varying(100),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- TOC entry 174 (class 1259 OID 16422)
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2066 (class 0 OID 0)
-- Dependencies: 174
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- TOC entry 173 (class 1259 OID 16409)
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    email character varying(320) NOT NULL,
    password character varying(60) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    remember_token character varying(100)
);


--
-- TOC entry 172 (class 1259 OID 16407)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2067 (class 0 OID 0)
-- Dependencies: 172
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 1917 (class 2604 OID 16446)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY basket ALTER COLUMN id SET DEFAULT nextval('basket_id_seq'::regclass);


--
-- TOC entry 1921 (class 2604 OID 16464)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_cart ALTER COLUMN id SET DEFAULT nextval('order_cart_id_seq'::regclass);


--
-- TOC entry 1919 (class 2604 OID 16455)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- TOC entry 1916 (class 2604 OID 16435)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY product_types ALTER COLUMN id SET DEFAULT nextval('product_types_id_seq'::regclass);


--
-- TOC entry 1915 (class 2604 OID 16427)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- TOC entry 1914 (class 2604 OID 16412)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2050 (class 0 OID 16443)
-- Dependencies: 179
-- Data for Name: basket; Type: TABLE DATA; Schema: public; Owner: -
--

COPY basket (id, user_id, prod_id, prod_type_id, quantity, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2068 (class 0 OID 0)
-- Dependencies: 178
-- Name: basket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('basket_id_seq', 19, true);


--
-- TOC entry 2054 (class 0 OID 16461)
-- Dependencies: 183
-- Data for Name: order_cart; Type: TABLE DATA; Schema: public; Owner: -
--

COPY order_cart (id, prod_name, type_name, quantity, order_id, created_at, updated_at) FROM stdin;
9	Карандаш	\N	1	7	2015-02-23 17:24:04	2015-02-23 17:24:04
10	Рабочая тетрадь	В линейку	1	8	2015-02-23 17:24:21	2015-02-23 17:24:21
11	Рабочая тетрадь	В клетку	1	8	2015-02-23 17:24:21	2015-02-23 17:24:21
12	Ручка	Красная паста	2	9	2015-02-23 17:26:15	2015-02-23 17:26:15
13	Карандаш	\N	3	10	2015-02-23 21:19:42	2015-02-23 21:19:42
14	Ручка	Красная паста	2	10	2015-02-23 21:19:43	2015-02-23 21:19:43
\.


--
-- TOC entry 2069 (class 0 OID 0)
-- Dependencies: 182
-- Name: order_cart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('order_cart_id_seq', 14, true);


--
-- TOC entry 2052 (class 0 OID 16452)
-- Dependencies: 181
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: -
--

COPY orders (id, user_id, status, created_at, updated_at) FROM stdin;
7	3	1	2015-02-23 17:24:04	2015-02-23 17:24:04
8	3	1	2015-02-23 17:24:21	2015-02-23 17:24:21
9	3	1	2015-02-23 17:26:15	2015-02-23 17:26:15
10	3	1	2015-02-23 21:19:42	2015-02-23 21:19:42
\.


--
-- TOC entry 2070 (class 0 OID 0)
-- Dependencies: 180
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('orders_id_seq', 10, true);


--
-- TOC entry 2048 (class 0 OID 16432)
-- Dependencies: 177
-- Data for Name: product_types; Type: TABLE DATA; Schema: public; Owner: -
--

COPY product_types (id, name, quantity, prod_id, created_at, updated_at) FROM stdin;
4	Красная паста	15	2	\N	2015-02-23 21:19:43
3	Синяя паста	15	2	\N	2015-02-23 17:04:49
2	В линейку	15	1	\N	2015-02-23 17:24:21
1	В клетку	15	1	\N	2015-02-23 17:24:21
\.


--
-- TOC entry 2071 (class 0 OID 0)
-- Dependencies: 176
-- Name: product_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('product_types_id_seq', 4, true);


--
-- TOC entry 2046 (class 0 OID 16424)
-- Dependencies: 175
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: -
--

COPY products (id, name, quantity, photo, created_at, updated_at) FROM stdin;
2	Ручка	\N	0002960.jpg	\N	\N
1	Рабочая тетрадь	\N	100010.png	\N	\N
3	Карандаш	20	M0000000770.jpg	\N	2015-02-23 21:19:43
\.


--
-- TOC entry 2072 (class 0 OID 0)
-- Dependencies: 174
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('products_id_seq', 3, true);


--
-- TOC entry 2044 (class 0 OID 16409)
-- Dependencies: 173
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY users (id, name, email, password, created_at, updated_at, remember_token) FROM stdin;
5	user2	user2@test.comm	$2y$10$66YiRImMtoIQ2uS5YsZnveOWtqrV6tM5qJgvorElrAlXHSUf.uoeC	2015-02-21 09:48:19	2015-02-21 09:48:23	ua0TMcOeqKUbWDYxyAMQj5InW1VJEA96lQXdVWVB6Z1GYhCYLgwO7dqtP2ue
4	user1	user1@test.comm	$2y$10$/5aEQQdznP5FUsU9HBXW2udlPJGC2HH4RfzXfkbDLWGFEFThlRDoq	2015-02-21 09:47:55	2015-02-23 15:48:09	aUaSze4SXYPRyNxkNNh5cn0YbM63MFGpvTCW3cKlrbrMQk45INx22K00Rg5z
3	admin	lemi.amart@gmail.com	$2y$10$e6/Cy2BpYlqR5YWrUOHZqeealsJAAnmlxBTm.BT7URT9FBFkNB.Fi	2015-02-21 09:22:30	2015-02-23 22:01:17	wlCag5gR9j93mTJAeNr61WBNX3DFPy7vHRjxul56uVC5o0Qj9Pzab2kmSNzE
\.


--
-- TOC entry 2073 (class 0 OID 0)
-- Dependencies: 172
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_id_seq', 5, true);


--
-- TOC entry 1929 (class 2606 OID 16448)
-- Name: basket_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY basket
    ADD CONSTRAINT basket_pkey PRIMARY KEY (id);


--
-- TOC entry 1933 (class 2606 OID 16469)
-- Name: order_cart_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY order_cart
    ADD CONSTRAINT order_cart_pkey PRIMARY KEY (id);


--
-- TOC entry 1931 (class 2606 OID 16458)
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- TOC entry 1927 (class 2606 OID 16440)
-- Name: product_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY product_types
    ADD CONSTRAINT product_types_pkey PRIMARY KEY (id);


--
-- TOC entry 1925 (class 2606 OID 16429)
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- TOC entry 1923 (class 2606 OID 16414)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


-- Completed on 2015-02-23 21:29:25

--
-- PostgreSQL database dump complete
--

