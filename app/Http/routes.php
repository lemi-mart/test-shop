<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SiteController@catalog');

//Route::get('home', 'HomeController@index');
Route::get('profile', 'OrdersController@OrderList');
Route::get('catalog', 'SiteController@catalog');
Route::get('product/{id}', 'SiteController@product');
Route::get('basket', 'BasketController@index');

Route::post('basket/add', 'BasketController@addItem');
Route::post('basket/update', 'BasketController@updateItem');
Route::post('basket/delete', 'BasketController@removeItem');

Route::post('order/new', 'OrdersController@newOrder');
Route::post('order/filter', 'OrdersController@filterOrders');

//Route::match(
//    array('GET', 'POST'),'test');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
