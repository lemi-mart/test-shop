<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use View;
use  App\Models as Models;

class MainController extends Controller {
	
	public $basket_items = 0;//количество товаров в корзине
	
	public function __construct()
	{
		$this->middleware('auth');
	}

//	protected function getBasketItems(){
//		$basket_items = 
//	}
	protected function renderView($view,$data=array()){
		return view('layouts.main')->with('view', View::make($view,$data));
	}

}
