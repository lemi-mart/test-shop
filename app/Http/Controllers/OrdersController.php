<?php namespace App\Http\Controllers;

use Auth;
use App\Models as Models;
use View;
use Request;

class OrdersController extends MainController {
	public function __construct()
	{
		$this->middleware('auth');
		if(!Auth::guest()){			
			$basket_items = Models\Basket::getBasketQuantity(Auth::id());
			View::share('basket_items', $basket_items);
		}
	}
	public function newOrder() {
		$target_quantity = 0;
		$prod_type = null;
		$product_id = null;
		
		$user_id = (int)Auth::id();
		$items = Models\Basket::getBasketItems($user_id);
		$order = new Models\Order();
		$order->user_id = $user_id;
		$order->status = 1;
		
		if ($order->save()) {
			foreach ($items as &$item) {
				//проверка наличия товара на складе
				if (!empty($item->type_id)) {
					$target_product = Models\ProductTypes::find($item->type_id);
					$target_quantity = $target_product->quantity;
				} else if (!empty($item->product_id)) {
					$target_product = Models\Product::find($item->product_id);
					$target_quantity = $target_product->quantity;
				}
				
				$basket_quantity = $target_quantity >= $item->basket_quantity ? $item->basket_quantity : $target_quantity; // проверка соответствия количества
				$orderCart = new Models\OrderCart();

				$orderCart->prod_name = $item->product;
				$orderCart->type_name = $item->type_name;
				$orderCart->quantity = $basket_quantity;
				$orderCart->order_id = $order->id;
				if($orderCart->save()){
					$target_product->quantity = $target_quantity - $basket_quantity;
					$target_product->save();
				}
			}
			Models\Basket::where('user_id', '=', $user_id)->delete();
			$basket_items = Models\Basket::getBasketQuantity($user_id);
			$view = View::make('order-success',array('order_id'=>$order->id))->__toString();
			echo json_encode(array('status'=>true,'view'=>$view,'basket_items'=>$basket_items));	
		} else {
			$basket_items = Models\Basket::getBasketQuantity($user_id);
			$view = View::make('order-fail')->__toString();
			echo json_encode(array('status'=>false,'view'=>$view,'basket_items'=>$basket_items));	
		}
	}
	public function OrderList() {
		$user_id = (int)Auth::id();
		$orders = Models\Order::getOrders($user_id);
		return view('profile',array('orders_list'=>$orders));
	}
	public function filterOrders() {
		$user_id = (int)Auth::id();
		$date_from = Request::input('date_from');
		$date_to = Request::input('date_to');
		$count = (int)Request::input('count');
		
		$orders = Models\Order::filterOders($user_id,$date_from,$date_to,$count);
		$view = View::make('orders',array('orders_list'=>$orders))->__toString();
		echo json_encode(array('status'=>true,'view'=>$view));
	}
}