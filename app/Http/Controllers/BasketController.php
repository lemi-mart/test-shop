<?php namespace App\Http\Controllers;

use Auth;
use App\Models as Models;
use View;
use Request;

class BasketController extends MainController {

	public function __construct()
	{
		$this->middleware('auth');
		if(!Auth::guest()){			
			$basket_items = Models\Basket::getBasketQuantity(Auth::id());
			View::share('basket_items', $basket_items);
		}
	}

	public function index()
	{	$items = array();
		$items = Models\Basket::getBasketItems(Auth::id());
		return view('basket',array('items'=>$items));
	}
	
	public function addItem() {
		$target_quantity = 0; // количество товара на складе		
		$success = true;
		$data = array();//данные добавляемого товара
		$msg = '';//текст в json ответе
		
		$data['user_id'] = (int)Auth::id();
		$data['product_id'] = (int)Request::input('product_id');
		$data['prod_type'] = (int)Request::input('prod_type');
		$data['quantity'] = (int)Request::input('quantity');
		
		//проверка наличия товара на складе
		if (!empty($data['prod_type'])) {
			$target_quantity = Models\ProductTypes::find($data['prod_type'])->quantity;
		} else if (!empty($data['product_id'])) {
			$target_quantity = Models\Product::find($data['product_id'])->quantity;
		}
		
		if((int)$target_quantity > 0){
			$data['quantity'] = $target_quantity >= $data['quantity']?$data['quantity']:$target_quantity; // проверка соответствия количества
			$item = array();
			
			if ($item = Models\Basket::getItem($data)) {
				$success = true;
				if (Models\Basket::updateItem($item, $data)) {
					$basket_items = Models\Basket::getBasketQuantity(Auth::id());
				} else {
					$success = false;
					$msg = 'Ошибка добавления в корзину';
				}
			} else {
				if (Models\Basket::addItem($data)) {
					$basket_items = Models\Basket::getBasketQuantity(Auth::id());
				} else {
					$success = false;
					$msg = 'Ошибка добавления в корзину';
				}
			}
		} else {
			$success = false;
			$msg = 'Товар закончился на складе';
		}
		echo json_encode(array('status'=>$success,'msg'=>$msg,'basket_items'=>$basket_items));		
	}
	
	public function updateItem() {
		$data = array();
		$id = (int) Request::input('item_id');
		$data['quantity'] = (int) Request::input('quantity');
		$user_id = (int) Auth::id();

		$item = Models\Basket::find($id);
		//проверка наличия товара на складе
		if (!empty($item->prod_type_id)) {
			$target_quantity = Models\ProductTypes::find($item->prod_type_id)->quantity;
		} else if (!empty($item->prod_id)) {
			$target_quantity = Models\Product::find($item->prod_id)->quantity;
		}
		$data['quantity'] = $target_quantity >= $data['quantity']?$data['quantity']:$target_quantity; // проверка соответствия количества
		
		if ($item->user_id == $user_id) {
			if (Models\Basket::updateItem($item, $data)) {
				$success = true;
				$msg = 'ок';
			} else {
				$success = false;
				$msg = 'error';
			}
			$basket_items = Models\Basket::getBasketQuantity(Auth::id());
			echo json_encode(array('status' => $success, 'msg' => $msg,'basket_items'=>$basket_items));
		} else {
			echo json_encode(array('status' => false, 'msg' => 'error','basket_items'=>$basket_items));
		}
	}
	
	public function removeItem() {
		$id = (int) Request::input('item_id');
		$user_id = (int) Auth::id();
		$item = Models\Basket::find($id);
		if ($item->user_id == $user_id) {
			if ($item->delete()) {
				$success = true;
				$msg = 'ок';
			} else {
				$success = false;
				$msg = 'error';
			}
			$basket_items = Models\Basket::getBasketQuantity($user_id);
			echo json_encode(array('status' => $success, 'msg' => $msg, 'basket_items' => $basket_items));
		} else {
			$basket_items = Models\Basket::getBasketQuantity(Auth::id());
			echo json_encode(array('status' => false, 'msg' => 'error', 'basket_items' => $basket_items));
		}
	}

}
