<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use View;
use App\Models as Models;
use Session;


class SiteController extends MainController {
	
	
	public function __construct()
	{
		$this->middleware('auth');	
		if(!Auth::guest()){			
			$basket_items = Models\Basket::getBasketQuantity(Auth::id());
			View::share('basket_items', $basket_items);
		}
		
	}
	
	
	public function catalog() {		
		$products = Models\Product::all();
		return view('catalog',array('products'=>$products));
	}
	public function profile() {
		return view('profile');
	}
	public function product($id) {
		if($products = Models\Product::getProduct($id)){
			$is_available = false;
			foreach ($products as $prod){
				if (!$prod->quantity) {
					if ($prod->type_quantity) {
						$is_available = true;
					}
				} else {
					$is_available = true;
				}
			}
			return view('product',array('product_id'=>$id, 'products'=>$products,'is_available'=>$is_available));
		} else {
			abort(404);
		}
		
	}


}
