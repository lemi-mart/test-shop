<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use DB;

class Order extends Eloquent {

	protected $table = 'orders';

	public static function getOrders($id) {
		$orders = array();
		$items = DB::table('orders AS o')
				->join('order_cart AS oc','oc.order_id', '=', 'o.id')
				->where('o.user_id', '=', $id)
				->orderBy('o.id','DESC')
				->select('o.id','oc.prod_name','oc.type_name','oc.quantity','o.created_at')->get();
		foreach ($items as $item){
			$orders[$item->id][] = $item;
		}
		return $orders;
	}
	
	/*Фильтр заказов*/
	public static function filterOders($user_id,$date_from='',$date_to='',$count=1) {
		if ($date_to) {
			$date = new \DateTime($date_to.' 23:59:59');
			$date_to =  $date->format('Y-m-d H:i:s');
		} else {
			$date = new \DateTime(date('d.m.Y').' 23:59:59');
			$date_to =  $date->format('Y-m-d H:i:s');
		}
		if ($date_from) {
			$date = new \DateTime($date_from);
			$date_from =  $date->format('Y-m-d H:i:s');
		} else{
			$date = new \DateTime('01.01.1979'.' 00:00:59');
			$date_from =  $date->format('Y-m-d H:i:s');
		}

		$orders = array();
		$items = DB::select( 'SELECT o.id, oc.prod_name ,oc.type_name, oc.quantity,o.created_at FROM orders O 
				LEFT JOIN order_cart oc ON O.id = OC.order_id 
				WHERE OC.order_id IN (
						SELECT O.id FROM orders O 
						LEFT JOIN order_cart OC ON O.id = OC.order_id 
						GROUP BY O.id HAVING SUM(OC.quantity) = ?)
				AND  o.user_id = ? AND o.created_at >= ? AND o.created_at <= ? ORDER BY o.id DESC',[$count,$user_id,$date_from,$date_to]);

		foreach ($items as $item){
			$orders[$item->id][] = $item;
		}
		return $orders;
	}

}
