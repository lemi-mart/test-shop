<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTypes extends Model {

    protected $table = 'product_types';
	
	public function product() {
		return $this->belongsTo('App\Models\Product', 'id');
	}

}