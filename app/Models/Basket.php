<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use DB;

class Basket extends Eloquent {
	protected $table = 'basket';
	
	
	public static function getBasketQuantity($id) {
		$total_quantity = 0;
		$items = Basket::User_id($id)->get();
		foreach($items as $item){
			$total_quantity += $item->quantity;
		}
		return $total_quantity;
	}
	
	public static function getBasketItems($id) {
		$items = DB::table('basket AS b')
				->join('products AS p', 'p.id', '=', 'b.prod_id')
				->leftJoin('product_types AS pt', 'pt.id', '=', 'b.prod_type_id')				
				->where('b.user_id','=',$id)
				->select('b.id AS item_id', 'p.id AS product_id','pt.id AS type_id', 'p.name as product','pt.name AS type_name','p.photo',
					'p.quantity AS quantity','pt.quantity AS type_quantity','b.quantity AS basket_quantity')->get();
		return $items;
	}
	
	//получение товара из корзины
	public static function getItem($data) {
		return $item = Basket::Product_id($data['product_id'])->User_id($data['user_id'])->Prod_type_id($data['prod_type'])->first();
	}
	
	public static function addItem($data){
		$item = new Basket();
		$item->user_id = $data['user_id'];
		$item->prod_id = $data['product_id'];
		$item->prod_type_id = $data['prod_type'];
		$item->quantity = $data['quantity'];
		return $item->save();
	}
	
	public static function updateItem(Basket $item,$data) {
		$item->quantity = $data['quantity'];
		return $item->save();
	}
	
	//Динамические скоупы 
	public function scopeProduct_id($query, $product_id) {
		return $query->where('prod_id', '=', $product_id);
	}

	public function scopeUser_id($query, $user_id) {
		return $query->where('user_id', '=', $user_id);
	}

	public function scopeProd_type_id($query, $prod_type_id) {
		return $query->where('prod_type_id', '=', $prod_type_id);
	}
}