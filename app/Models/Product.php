<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use DB;


class Product extends Eloquent {

	


    protected $table = 'products';
	
	public function ProdTypes() {
		return $this->hasMany('App\Models\ProductTypes','prod_id');
	}
	
	public static function getProduct($id) {
		$product = DB::table('products')->where('id','=',$id)->whereNotNull('quantity')->get();
		if(!$product){
			return $product = DB::table('products AS p')
				->leftJoin('product_types AS pt', 'p.id', '=', 'pt.prod_id')
				->where('p.id','=',$id)
//				->where('pt.quantity','>',0)
				->select('p.id AS product_id', 'pt.id AS type_id',
					'p.name', 'pt.name AS type_name','p.photo',
					'pt.quantity AS type_quantity', 'p.quantity AS quantity')
				->get();
		} else {
			return $product;
		}
//		return Product::find($id)->whereNotNull('quantity');
//		return Product::whereNotNull('quantity');
//		return Product::find($id)->productTypes;
	}

}