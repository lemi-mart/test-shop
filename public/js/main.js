$(function() {
	$('.datepicker').datepicker({dateFormat: "dd.mm.yy",
		showOtherMonths: true,
		selectOtherMonths: true,
		changeYear: true,
		changeMonth: true,
		yearRange: '2000:2099'});
	
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '&#x3c;Пред',
		nextText: 'След&#x3e;',
		currentText: 'Сегодня',
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
			'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
			'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
		dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
		dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		weekHeader: 'Не',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['ru']);
	
	
	$('#add_to_basket').on('click', function() {
		var prod_type = $('#type_id').length ? $('#type_id').val() : null;
		var csrf = $('#csrf_token').length ? $('#csrf_token').val() : null;
		var prod_id = $('#product_id').length ? $('#product_id').val() : null;
		var quantity = $('#quantity').length ? $('#quantity').val() : null;
		$.ajax({
			url: '/basket/add',
			dataType: 'json',
			data: {
				_token: csrf,
				product_id: prod_id,
				prod_type: prod_type,
				quantity: quantity
			},
			type: 'POST',
			success: function(res) {
				$('#basket_info').text('Товаров в корзине: '+res.basket_items);
			}
		})
	});
	
	$('.item_quantity').on('change',function(){
		var quantity = $(this).val();
		var item_id = $(this).closest('ul').attr('data-id');
		var csrf = $('#csrf_token').val();
		
		$.ajax({
			url: '/basket/update',
			dataType: 'json',
			data: {
				_token: csrf,
				item_id: item_id,
				quantity: quantity
			},
			type: 'POST',
			success: function(res) {
				$('#basket_info').text('Товаров в корзине: '+res.basket_items);
			}
		})
	});
	
	$('.remove').on('click',function(){
		var csrf = $('#csrf_token').val();
		var item_id = $(this).closest('ul').attr('data-id');
		$.ajax({
			url: '/basket/delete',
			dataType: 'json',
			data: {
				_token: csrf,
				item_id: item_id
			},
			type: 'POST',
			success: function(res) {
				$('#basket_info').text('Товаров в корзине: '+res.basket_items);
				if(res.basket_items){
					$('#item_' + item_id).remove();
				} else {
					$('.items_list').html('Корзина пуста');
				}
			}
		})
	});
	
	$('#go_order').on('click',function(){
		var csrf = $('#csrf_token').val();
		if (confirm('Оформить заказ?')) {
			$.ajax({
				url: '/order/new',
				dataType: 'json',
				type: 'POST',
				data: {
					_token: csrf
				},
				success: function(res) {
					$('.basket').html(res.view);
					$('#basket_info').text('Товаров в корзине: '+res.basket_items);
				}
			})
		}
	});
	$('#go_filter').on('click',function(){
		var csrf = $('#csrf_token').val();
		$.ajax({
			url: '/order/filter',
			dataType: 'json',
			type: 'POST',
			data: {
				_token: csrf,
				date_from:$('#date_from').val(),
				date_to:$('#date_to').val(),
				count:$('#count_items').val()
			},
			success: function(res) {
				$('#orders_list').html(res.view);
//				$('#basket_info').text('Товаров в корзине: ' + res.basket_items);
			}
		});
	});
});